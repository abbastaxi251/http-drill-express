const express = require("express")
const fs = require("fs")
const uuid = require("uuid")
const config = require("./config")

const port = config.port
const app = express()

app.use((req, res, next) => {
    const logMessage = `Date : ${new Date().toISOString()} | METHOD : ${
        req.method
    } | URL : ${req.hostname}:${port}${req.url}\n`
    fs.appendFile(__dirname + "/requestLogs.log", logMessage, (err) => {
        if (err) {
            console.error("Error appending to the log file")
        } else {
            next()
        }
    })
})

app.get(`/html`, (req, res, next) => {
    console.log(req.rawHeaders)
    fs.readFile(__dirname + "/index.html", (err, htmlContent) => {
        if (err) {
            console.error("error reading the html file")
            next(err)
        } else {
            res.setHeader("200", { "Content-Type": "text/html" })
            res.write(htmlContent)
            res.end()
        }
    })
})

app.get(`/json`, (req, res, next) => {
    fs.readFile(__dirname + "/data.json", (err, data) => {
        if (err) {
            console.error("error reading the html file")
            next(err)
        } else {
            res.setHeader("200", { "Content-Type": "text/json" })
            res.write(data)
            res.end()
        }
    })
})

app.get(`/uuid`, (req, res) => {
    res.setHeader("200", { "Content-Type": "text/json" })
    res.write(JSON.stringify({ uuid: uuid.v4() }))
    res.end()
})

app.get(`/status/:statusCode`, (req, res) => {
    const statusCode = req.params.statusCode
    res.setHeader("Content-Type", "text/plain")
    res.status(statusCode)
    res.write(`Response with status code ${statusCode}`)
    res.end()
})

app.get(`/delay/:delayTime`, (req, res) => {
    const delayTime = req.params.delayTime
    setTimeout(() => {
        res.setHeader("200", { "Content-Type": "text/plain" })
        res.write(`Response received after ${delayTime} seconds`)
        res.end()
    }, delayTime * 1000)
})

app.get(`/logs`, (req, res, next) => {
    fs.readFile(__dirname + "/requestLogs.log", "utf-8", (err, data) => {
        if (err) {
            console.error("error reading the log file")
            next(err)
        } else {
            res.setHeader("200", { "Content-Type": "text/plain" })
            res.write(data)
            res.end()
        }
    })
})

app.listen(port, () => {
    console.log(`listening on port ${port}`)
})
